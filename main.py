from urllib.parse import urlencode, quote_plus
from urllib.request import urlopen
from bs4 import BeautifulSoup
import time
import xml.etree.ElementTree as ET
import operator
from datetime import datetime


########################################
###CONFIG
########################################
pathXml = 'C:/Users/ ... nlp4fun_test_10092018_v2.xml'
fname = '... myStopwords.txt'
outputTxt = '... output.txt'
########################################
########################################


def possibiliSoluzioni(clues, size = "1000000000"):
    #clues is an array of 5 clues + id
    #size are the total results. The higher they are, the more precise the program is, but slow

    corpora = ["repubblica","itwac_full"];
    testo = ["","","","",""]; #here we get the results of the 5 queries

    for i in range(0,5):
        
        cluesParsato = quote_plus(clues[i].encode('utf-8'), safe=':/'.encode('utf-8'))
        #urlA and urlB, one for corpora
        urlA = "https://corpora.dipintra.it/public/run.cgi/first?corpname="+corpora[0]+"&pagesize="+size+"&reload=&iquery="+cluesParsato+"&queryselector=iqueryrow&lemma=&phrase=&word=&char=&cql=&default_attr=word&fc_lemword_window_type=both&fc_lemword_wsize=5&fc_lemword=&fc_lemword_type=all&usesubcorp=&fsca_text.id=&fsca_text.wordcount=";
        urlB = "https://corpora.dipintra.it/public/run.cgi/first?corpname="+corpora[1]+"&pagesize="+size+"&reload=&iquery="+cluesParsato+"&queryselector=iqueryrow&lemma=&phrase=&word=&char=&cql=&default_attr=word&fc_lemword_window_type=both&fc_lemword_wsize=5&fc_lemword=&fc_lemword_type=all&usesubcorp=&fsca_text.id=&fsca_text.wordcount=";

        pageA = urlopen(urlA)
        pageB = urlopen(urlB)

        soupA = BeautifulSoup(pageA, "html.parser")
        soupB = BeautifulSoup(pageB, "html.parser")

        #I combine the text extracted from the two corpora in text[i] (i goes from 0 to 4, so we would have a text for each clue)
        tableA = soupA.find('table')
        for rowA in tableA.findAll("tr"):
            cells = rowA.findAll("span")
            if (cells == []):
                break;
            testo[i] += cells[0].text.lower()
            testo[i] += " "
            testo[i] += cells[1].text.lower()


        tableB = soupB.find('table')
        for rowB in tableB.findAll("tr"):
            cells = rowB.findAll("span")
            if (cells == []):
                break;
            testo[i] += cells[0].text.lower()
            testo[i] += " "
            testo[i] += cells[1].text.lower()

        #in case the text is empty (ie nothing is found)
        if (str(len(testo[i])) == 0):
            return []

    #split words
    words1 = testo[0].split()
    words2 = testo[1].split()
    words3 = testo[2].split()
    words4 = testo[3].split()
    words5 = testo[4].split()


    #intersection
    words12 = set(words1).intersection(words2)
    words123 = set(words12).intersection(words3)
    words1234 = set(words123).intersection(words4)
    words12345 = set(words1234).intersection(words5)

    #the variable fname has been declared at the beginning and contains the path of the file with the stopwords
    with open(fname, encoding='utf-8') as f:
        content = f.readlines()

    stopWord = [x.strip() for x in content] 

    soluzioni = words12345.difference(stopWord)
   
    return soluzioni


def scaricaProverbi(indizio):
    numeroPagina = 1 #page number of the website
    testoProverbi = ""

    try:
        while(True):
            paginaProverbi = urlopen("http://www.proverbi-italiani.org/ricerca_libera_ris_1.asp?parola1="+str(indizio)+str("&IDP=")+str(numeroPagina))
            soupProverbi = BeautifulSoup(paginaProverbi, "html.parser")

            proverbi = soupProverbi.findAll("td", {"class": "destra proverbio"})

            if (proverbi == []):
                return testoProverbi

            for i in proverbi:
                testoProverbi += i.text;
                testoProverbi += "\n";

        
            numeroPagina += 1
    except:
        return ""
def scaricaDefinizioneDizionario(indizio):

    try:
        paginaDizionario = urlopen("http://www.grandidizionari.it/Dizionario_Italiano/cerca.aspx?query="+str(indizio))
        soupDizionario = BeautifulSoup(paginaDizionario, "html.parser")

        definizione = soupDizionario.findAll("meta", {"id": "head_description"})
    
        if (definizione == []):
            return ""

        return str(definizione)
    except:
        return ""
def scaricaWikiQuote(indizio):

    try:
        paginaWiki = urlopen("https://it.wikiquote.org/wiki/"+str(indizio))
        soupWiki = BeautifulSoup(paginaWiki, "html.parser")

        contenuto = soupWiki.findAll("div", {"class": "mw-parser-output"})

        listaFrasi = contenuto[0].findAll("li")

        testoDaRestituire = ""
        for abc in listaFrasi:
            testoDaRestituire += abc.text
            testoDaRestituire += "\n"
    
        return testoDaRestituire
    except:
        return ""
def scaricaFilm(indizio):

    try:
        paginaImdb = urlopen("https://www.imdb.com/find?q="+str(indizio)+str("&s=tt&ref_=fn_al_tt_mr"))
        soupImdb = BeautifulSoup(paginaImdb, "html.parser")

        contenuto = soupImdb.findAll("table", {"class": "findList"})

        listaFilm = contenuto[0].findAll("td", {"class": "result_text"})

        testoDaRestituire = ""
        for abc in listaFilm:
            testoDaRestituire += abc.text
            testoDaRestituire += "\n"
    
        return testoDaRestituire
    except:
        return ""
def scaricaLibro(indizio):

    try:
        paginaIbs = urlopen("https://www.ibs.it/search/?ts=as&query="+str(indizio)+str("&qs=true&products_per_page=200"))
        soupIbs = BeautifulSoup(paginaIbs, "html.parser")

        contenuto = soupIbs.findAll("div", {"class": "title"})

        testoDaRestituire = ""

        for abc in contenuto:
            testoDaRestituire += abc.text
            #testoDaRestituire += "\n"  do not need it, it goes by itself, because of the output
    
        return testoDaRestituire
    except:
        return ""
def creaPack(w):
    pack = ""
    pack += scaricaProverbi(w)
    pack += scaricaDefinizioneDizionario(w)
    pack += scaricaWikiQuote(w)
    pack += scaricaFilm(w)
    pack += scaricaLibro(w)

    #split words
    rePack = pack.split();

    return rePack
def initPesi(lista):
    #add a weight of value 0 to each word
    nuovaLista = []

    for abc in lista:
        new = [abc, 0]
        nuovaLista.append(new)
    return nuovaLista
def attivazione(indizi, possSoluzioni):
    print("Attivazione")

    pack1 = creaPack(indizi[0])
    pack2 = creaPack(indizi[1])
    pack3 = creaPack(indizi[2])
    pack4 = creaPack(indizi[3])
    pack5 = creaPack(indizi[4])

    possSoluzioniPesate = initPesi(possSoluzioni)

    for w in possSoluzioniPesate:   # if a single possible solution (w) is found in the pack (1,2..5) (set of #provers, 
                                    # definitions ... inherent to one of the 5 clues) then the weight relative to that 
                                    # word increases by 1
        if (w[0] in pack1):
            w[1] += 1
        if (w[0] in pack2):
            w[1] += 1
        if (w[0] in pack3):
            w[1] += 1
        if (w[0] in pack4):
            w[1] += 1
        if (w[0] in pack5):
            w[1] += 1

    soluzioniFinali = []

    possSoluzioniPesateOrdinate = []
    possSoluzioniPesateOrdinate = sorted(possSoluzioniPesate, key=operator.itemgetter(1), reverse=True)

    soluzioniFinali = possSoluzioniPesateOrdinate[:100] #I only take the first 100 solutions found
    return soluzioniFinali
def importData(path):
    tree = ET.parse(path)
    root = tree.getroot()

    gameList=[]

    i = 0
    for child in root:
        gameList.append([root[i][1].text,root[i][2].text,root[i][3].text,root[i][4].text,root[i][5].text,root[i][0].text])
        i+=1

    return gameList






# imports the data
##gameList = importData(pathXml)
gameList = [
    ["onde","stazione","antenna","frequenza","osso","id"]
]


for clue in gameList:
    startTime = datetime.now()


    parole = possibiliSoluzioni(clue,"8000")
    soluzioni = attivazione(clue, parole)

    stopTime = datetime.now()
    diff = (stopTime-startTime)
    elapsed_ms = int((diff.days * 86400000) + (diff.seconds * 1000) + (diff.microseconds / 8000))

    with open(outputTxt, 'a', encoding='utf-8') as the_file:
        j=1
        if (soluzioni):
            for line in soluzioni:
                #######id solution score rank time
                risultato = clue[5]+str(" ")+str(line[0])+str(" ")+str(line[1]/5)+str(" ")+str(j)+str(" ")+str(elapsed_ms)+str("""\n""")
                the_file.write(risultato)
                j+=1
            the_file.write("""\n""")
    the_file.close()

